var babel = require('rollup-plugin-babel');

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    eslint: {
      target: ['src/**/*.js']
    },
    rollup: {
      options: {
        plugins: [
          babel({
            exclude: './node_modules/**'
          })
        ]
      },
      files: {
        'dest': 'dist/bundle.js',
        'src': 'src/index.js', // Only one source file is permitted
      },
    },
    watch: {
      app: {
        options: {
          livereload: true
        },
        tasks: ['rollup'],
        files: [
          'src/*.js',
          'playground/*.html',
          'playground/js/*.js'
        ]
      }
    },
    connect: {
      app: {
        options: {
          port: 9500,
          livereload: true,
          base: ['playground', '.']
        }
      }
    },
    open: {
      app: {
        path: 'http://localhost:9500'
      }
    }
  });

  // Update the common library
  grunt.registerTask('build', ['eslint', 'rollup']);
  grunt.registerTask('serve', ['connect', 'open', 'watch']);
  grunt.registerTask('default', ['build', 'serve']);
}